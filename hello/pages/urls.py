from django.urls import path
from .views import home_page_view

app_name = "pages_urls"

urlpatterns = [
    path('', home_page_view, name="home_page"),
]
