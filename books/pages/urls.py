from django.urls import path
from .views import HomePageView


# pages urls lists...
# app_name = "pages_urls

urlpatterns = [
    path('', HomePageView.as_view(), name="homepage"),
]
